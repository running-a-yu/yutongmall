package com.atguigu.yutongmall.product;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class YutongmallProductApplication {

    public static void main(String[] args) {
        SpringApplication.run(YutongmallProductApplication.class, args);
    }

}
