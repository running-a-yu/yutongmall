package com.guigu.yutongmall.ware;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class YutongmallWareApplication {

    public static void main(String[] args) {
        SpringApplication.run(YutongmallWareApplication.class, args);
    }

}
