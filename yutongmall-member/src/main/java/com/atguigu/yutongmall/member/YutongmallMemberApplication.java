package com.atguigu.yutongmall.member;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class YutongmallMemberApplication {

    public static void main(String[] args) {
        SpringApplication.run(YutongmallMemberApplication.class, args);
    }

}
