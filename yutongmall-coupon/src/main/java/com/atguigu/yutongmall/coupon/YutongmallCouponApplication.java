package com.atguigu.yutongmall.coupon;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class YutongmallCouponApplication {

    public static void main(String[] args) {
        SpringApplication.run(YutongmallCouponApplication.class, args);
    }

}
