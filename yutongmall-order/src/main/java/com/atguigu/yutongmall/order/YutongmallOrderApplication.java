package com.atguigu.yutongmall.order;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class YutongmallOrderApplication {

    public static void main(String[] args) {
        SpringApplication.run(YutongmallOrderApplication.class, args);
    }

}
